/*jshint browser: true, devel: true */
/*jslint browser: true, nomen: true */

var Life = function (universe) {
    "use strict";
    // universe is an object that defines the Life object
    // rule is Life rule, as in "B3/S23"
    // maxHistory is maximum length of generations to keep, can be Infinity
    // initial generation is an array of arrays
    // width and height are size of the universe

    var rxRule = /^[bB](\d{1,8})\/[sS](\d{1,8})$/,
        result;

    result = {
        _init: function () {
            if (!this._checkRule(universe.rule)) {
                throw new Error("Can't create Life with bad rules");
            }
            this.universe = universe;
            this.universe.ruleBornIf = this._parseRule(null, 'B');
            this.universe.ruleSurvivesIf = this._parseRule(null, 'S');

            this.generations = [];
            this.generations = this._setGeneration(null, universe.initial || [[]]);

            this.drawMethod = this.universe.drawMethod || 'IfNeeded';
        },
        _checkRule: function (rule) {
            rule = rule || this.universe.rule;
            return rxRule.test(rule);
        },
        _parseRule: function (rule, sb) {
            rule = rule || this.universe.rule;
            var int = function (x) {return +x; },
                s = rxRule.exec(rule)[sb.toUpperCase() === 'B' ? 1 : 2];
            return s.split('').map(int);
        },
        _rotateHistory: function (generations, maxHistory) {
            generations = generations || this.generations;
            maxHistory = maxHistory || this.universe.maxHistory || Infinity;
            var l = generations.length;
            if (l >= maxHistory) {
                delete generations[l - maxHistory];
            }
            return generations;
        },
        _setGeneration: function (generations, generation) {
            generations = generations || this.generations;
            generations = this._rotateHistory();
            generations.push(generation);
            return generations;
        },

        rule: function () {
            return 'B' + this.universe.ruleBornIf.join('') + '/' +
                'S' + this.universe.ruleSurvivesIf.join('');
        },

        empty: function (createBlankGeneration) {
            var x, y,
                generation = null;

            this.generations = [];
            if (createBlankGeneration) {
                generation = [];
                for (x = 0; x < this.universe.width; x += 1) {
                    generation.push([]);
                    for (y = 0; y < this.universe.height; y += 1) {
                        generation[x].push(0);
                    }
                }
                this.generations.push(generation);
            }
            return generation;
        },

        initCanvas: function (canvasId) {
            this.canvas = document.getElementById(canvasId);
            this.canvas.width = this.universe.width;
            this.canvas.height = this.universe.height;
            this.context = this.canvas.getContext("2d");
            this.context.fillStyle = '#555555';
        },

        randomize: function (alivePercentage) {
            var gx, gy, generation;

            this.generations = [];
            generation = [];
            for (gx = 0; gx < this.universe.width; gx += 1) {
                generation.push([]);
                for (gy = 0; gy < this.universe.height; gy += 1) {
                    generation[gx].push(+(Math.random() < alivePercentage));
                }
            }

            this._setGeneration(null, generation);
            return generation;
        },

        draw: function (generation, method) {
            method = method || this.drawMethod;
            this['draw' + method](generation);
        },

        drawBasic: function (generation) {
            // draws a generation on a canvas, each cell gets drawn or erased
            // depending on if it's alive or dead

            var gx, gy,
                glx, gly, ctx;

            generation = generation || this.generations[this.generations.length - 1];
            glx = generation.length;
            ctx = this.context;

            ctx.clearRect(0, 0, glx, gly);

            for (gx = 0; gx < glx; gx += 1) {
                gly = generation[gx].length;
                for (gy = 0; gy < gly; gy += 1) {
                    if (generation[gx][gy] === 1) {
                        ctx.fillRect(gx, gy, 1, 1);
                    } else {
                        ctx.clearRect(gx, gy, 1, 1);
                    }
                }
            }
        },

        drawIfNeeded: function (generation, prevGeneration) {
            // draws a generation on a canvas by comparing
            // each cell with prevGeneration and does the
            // actual drawing only if there is a difference

            var gx, gy, glx, gly,
                ctx = this.context;

            generation = generation || this.generations[this.generations.length - 1];
            glx = generation.length;

            if (!prevGeneration) {
                try {
                    prevGeneration = this.generations[this.generations.length - 2];
                } catch (error) {}
            }

            if (!prevGeneration) {
                // if prevGeneration does not exist (there is no history)
                // fall back to drawBasic
                this.drawBasic(generation);
            } else {
                for (gx = 0; gx < glx; gx += 1) {
                    gly = generation[gx].length;
                    for (gy = 0; gy < gly; gy += 1) {
                        if (generation[gx][gy] !== prevGeneration[gx][gy]) {
                            if (generation[gx][gy] === 1) {
                                ctx.fillRect(gx, gy, 1, 1);
                            } else {
                                ctx.clearRect(gx, gy, 1, 1);
                            }
                        }
                    }
                }
            }
        },

        _countNeighbours: function (generation, x, y) {
            var result = 0,
                cgx = generation.length - 1, cgy = generation[0].length - 1;

            if (x > 0 && y > 0) result += generation[x - 1][y - 1];
            if (x > 0) result += generation[x - 1][y];
            if (x > 0 && y < cgy) result += generation[x - 1][y + 1];

            if (y > 0) result += generation[x][y - 1];
            // skip itself
            if (y < cgy) result += generation[x][y + 1];

            if (x < cgx && y > 0) result += generation[x + 1][y - 1];
            if (x < cgx) result += generation[x + 1][y];
            if (x < cgx && y < cgy) result += generation[x + 1][y + 1];

            return result;
        },

        computeNext: function (generation) {
            // computes next generation, stores it in history and returns it
            var currentGeneration = generation || this.generations[this.generations.length - 1],
                nextGeneration = [],
                x, y, neighbours,
                cgxLength = currentGeneration.length, cgyLength,
                ruleBornIf = this.universe.ruleBornIf,
                ruleSurvivesIf = this.universe.ruleSurvivesIf;

            for (x = 0; x < cgxLength; x += 1) {
                cgyLength = currentGeneration[x].length;
                nextGeneration[x] = [];
                for (y = 0; y < cgyLength; y += 1) {
                    nextGeneration[x].push(currentGeneration[x][y]);
                    neighbours = this._countNeighbours(currentGeneration, x, y);
                    if (currentGeneration[x][y] === 0 && ruleBornIf.indexOf(neighbours) >= 0) {
                        nextGeneration[x][y] = 1;
                    }
                    if (currentGeneration[x][y] === 1 && ruleSurvivesIf.indexOf(neighbours) === -1) {
                        nextGeneration[x][y] = 0;
                    }
                }
            }
            this._setGeneration(null, nextGeneration);
            return nextGeneration;
        },

        play: function (interval, method) {
            var fn = function (life) {
                return function () {
                    life.computeNext();
                    life.draw(null, method);
                };
            };
            method = method || this.drawMethod;
            this.playInterval = setInterval(fn(this), interval);
        },

        pause: function () {
            if (this.playInterval) {
                clearInterval(this.playInterval);
            }
        },

        getStats: function (generation) {
            var x, y, cgyLength,
                countAlive = 0, countDead = 0;
            generation = generation || this.generations[this.generations.length - 1];

            for (x = 0; x < generation.length; x += 1) {
                cgyLength = generation[x].length;
                for (y = 0; y < cgyLength; y += 1) {
                    if (generation[x][y] === 0) countDead++;
                    if (generation[x][y] === 1) countAlive++;
                }
            }
            return {
                count: countAlive + countDead,
                countDead: countDead,
                countAlive: countAlive,
                percentageAlive: countAlive / (countAlive + countDead)
            };
        },

        readPattern: function (content, debug) {
            var i, j,
                generation = this.empty(true),
                lines = content.split('\n'),
                centerX = Math.floor(this.universe.width / 2),
                centerY = Math.floor(this.universe.height / 2),
                nx, ny, px, py,
                line, match;

            for (i = 0; i < lines.length; i++) {
                line = lines[i];

                if (debug && /^#D/.test(line)) {
                    console.log(line);
                }

                match = /^#P ([\-,0-9]+) ([\-,0-9]+)/.exec(line);
                if (match) {
                    if (debug) {
                        console.log(match[1], match[2]);
                    }
                    nx = centerX + parseInt(match[1], 10);
                    ny = centerY + parseInt(match[2], 10);
                }

                if (line[0] === '.' || line[0] === '*') {
                    // match dot or asterisk
                    if (debug) {
                        console.log(line);
                    }
                    for (j = 0; j < line.length; j++) {
                        if (line[j] === '.' || line[j] === '*')
                            generation[nx + j][ny] = (line[j] === '.' ? 0 : 1);
                    }
                    ny++;
                }

                if (line === "") continue;
            }
            this.draw();
        }
    };
    result._init();
    return result;
};

var LifeImporter = function (life, url, options) {
    options = options || {};

    var xhr = new XMLHttpRequest(),
        debug = options.debug || false;

    xhr.onreadystatechange = function () {
        if (this.readyState === this.DONE) {
            if (this.status === 200 && this.response !== null) {
                life.readPattern(this.response, debug);
            }
        }
    };
    xhr.open("GET", url);
    xhr.send();
};
