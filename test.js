module("sanity");

test("basic sanity checks", function () {
    expect(4);

    ok(true, "it's true");
    equal(0, "", "zero is losely equal to empty string");
    strictEqual(0, 0, "zero is strictly equal to zero");
    raises(function () {throw Error();}, Error, "throws an Error");
});


module("life-private", {
    setup: function () {
        this.life = new Life({rule: "B1/S1"});
    }
});

test("Life._rotateHistory", function () {
    var aa = [[]],
        life = new Life({rule: 'b1/s1', maxHistory: 2, initial: aa });
    deepEqual(life.generations[0], aa, "new Life's 0th generation is equal to initial");

    life._setGeneration(null, aa);
    deepEqual(life.generations, [aa, aa ], "_setGeneration pushes new generation to history");

    life._setGeneration(null, aa);
    deepEqual(life.generations, [undefined, aa, aa ],
        "_setGeneration pushes new generation to history and _rotateHistory deletes first");

    life._setGeneration(null, aa);
    deepEqual(life.generations, [undefined, undefined, aa, aa ],
        "_setGeneration and _rotateHistory once again do the work");
});


module("life-instance", {
    setup: function () {
        this.life = new Life({rule: "B3/S23"});
    }
});

test("Life._init", function () {
    strictEqual(typeof this.life, "object", "life is an object");
    raises(function () {new Life();}, TypeError, "empty constructor should throw Error");
    strictEqual(this.life.rule(), "B3/S23", "life has expected value of rule()");
    deepEqual(this.life.universe.ruleBornIf, [3], "life bornIf rule has expected value");
    deepEqual(this.life.universe.ruleSurvivesIf, [2, 3], "life survivesIf rule has expected value");

    ok(this.life._checkRule("b2/s23"), "_checkRule method should return true");
    ok(this.life._checkRule("B1/S123"), "_checkRule method should return true" );
    strictEqual(this.life._checkRule("b2"), false, "b2 is not valid rule");

    deepEqual(this.life._parseRule("b2/s23", "b"), [2], "_parseRule should return [2]");
    deepEqual(this.life._parseRule("b2/s23", "s"), [2, 3], "_parseRule should return [2, 3]");
});

test("Life.empty", function () {
    var gen = [ [0, 1], [1, 0] ],
        result;

    result = this.life.empty();
    deepEqual(this.life.generations, []);
    strictEqual(result, null, "result of empty() should be null.");

    result = this.life.empty(true);
    deepEqual(result, [], "result of empty(true) should be [].");
    deepEqual(this.life.generations, [[]]);
});


module("life-generation", {
    setup: function () {
        this.life = new Life({rule: 'b3/s23'});
        this.testNeighbourCount = function (gen, x, y, expectedCount) {
            strictEqual(this.life._countNeighbours(gen, x, y), expectedCount,
                "Cell (" + x + ", " + y + ") should have " + expectedCount + " neighbours");
        };
    }
});

test("Life._countNeighbours", function () {
    var gen = [ [0, 1, 0],
                [0, 1, 0], 
                [0, 1, 0] ];
    this.testNeighbourCount(gen, 0, 0, 2);
    this.testNeighbourCount(gen, 0, 1, 1);
    this.testNeighbourCount(gen, 0, 2, 2);

    this.testNeighbourCount(gen, 1, 0, 3);
    this.testNeighbourCount(gen, 1, 1, 2);
    this.testNeighbourCount(gen, 1, 2, 3);

    this.testNeighbourCount(gen, 2, 0, 2);
    this.testNeighbourCount(gen, 2, 1, 1);
    this.testNeighbourCount(gen, 2, 2, 2);
});

test("Life._countNeighbours", function () {
    var gen = [ [0, 1, 0, 0],
                [0, 1, 0, 0], 
                [0, 0, 0, 0], 
                [0, 1, 0, 0] ];
    this.testNeighbourCount(gen, 0, 0, 2);
    this.testNeighbourCount(gen, 0, 1, 1);
    this.testNeighbourCount(gen, 0, 2, 2);
    this.testNeighbourCount(gen, 0, 3, 0);

    this.testNeighbourCount(gen, 1, 0, 2);
    this.testNeighbourCount(gen, 1, 1, 1);
    this.testNeighbourCount(gen, 1, 2, 2);
    this.testNeighbourCount(gen, 1, 3, 0);

    this.testNeighbourCount(gen, 2, 0, 2);
    this.testNeighbourCount(gen, 2, 1, 2);
    this.testNeighbourCount(gen, 2, 2, 2);
    this.testNeighbourCount(gen, 2, 3, 0);

    this.testNeighbourCount(gen, 3, 0, 1);
    this.testNeighbourCount(gen, 3, 1, 0);
    this.testNeighbourCount(gen, 3, 2, 1);
    this.testNeighbourCount(gen, 3, 3, 0);
});

test("Life.computeNext", function () {
    //expect(0);
    var gen = [ [0, 1, 0],
                [0, 1, 0], 
                [0, 1, 0] ];

    deepEqual(this.life.computeNext(gen), [[0, 0, 0], [1, 1, 1], [0, 0, 0] ]);
});


module("performance", {
    setup: function () {
        this.life = new Life({rule: 'b3/s23', width: 200, height: 200});
        this.fasterThan = function (report, maxTime) {
            ok(report.deltaTime < maxTime, 
                report.iterations + " iterations should run in less than " + maxTime + " ms. " +
                "Ran in " + report.deltaTime + " ms."
            );
        };
    }
});

test("time computeNext", function () {
    var t1, t2,
        i, report,
        iterationCount = 10, iterationTime = 15;

    this.life.randomize(0.1);

    t1 = new Date().getTime();
    for (i = 0; i < iterationCount; i += 1)
        this.life.computeNext();
    t2 = new Date().getTime();

    report = {iterations: i, startTime: t1, endTime: t2, deltaTime: t2 - t1 };
    this.fasterThan(report, iterationCount * iterationTime);
});


